#!/usr/bin/env python
import numpy as np

from scipy.ndimage import binary_fill_holes
from PIL import Image
from PIL.ImageQt import ImageQt
# from PIL import ImageEnhance
# from PIL.ImageOps import autocontrast
import sys


# Simple normalization
# Normalization of intensity inside the cell, or if using an unmasked ndarray, normalization of the whole image
# Maskedarrays are used for seeing specific cells
def normalise_array(ndarray):
    #To cover for masked/unmasked arrays
    ndarrayData = ndarray[~ndarray.mask].data if isinstance(
        ndarray, np.ma.MaskedArray) else ndarray

    linearisedData = np.ravel(ndarrayData)

    # Perform simple normalization
    maxInt = np.max(linearisedData)
    minInt = np.min(linearisedData)

    conversionDict = {}
    for val in linearisedData:
        conversionDict[val] = int(
            np.round(255 * ((val - minInt) / (maxInt - minInt))))

    f = lambda x: conversionDict[x] if x in conversionDict.keys() else 0
    vectorizedFun = np.vectorize(f)
    result = vectorizedFun(ndarray)

    # If it is a maskedArray, return the whole image with the masked part as 0's
    wholeResult = result.filled(0) if isinstance(ndarray,
                                                 np.ma.MaskedArray) else result

    return wholeResult


def generateMaskedArrays(ndarray, segOutline, mainMetadata, segOutMetadata):
    nCells = segOutMetadata['nCells']  # Obtain this later with metadata
    nStacks = mainMetadata['nStacks']  # idem
    outlineWidth = segOutMetadata['width']  # idem

    outlineIndivWidth = int(outlineWidth / nCells)
    filledOutlines = binary_fill_holes(segOutline).astype(int)

    masks = [
        filledOutlines[:, i * outlineIndivWidth:(i + 1) *
                       outlineIndivWidth].astype(bool) for i in range(nCells)
    ]
    tiledMasks = [np.tile(mask, nStacks) for mask in masks]
    maskedCells = [
        np.ma.array(np.matrix(ndarray), mask=~mask, fill_value=0)
        for mask in tiledMasks
    ]

    return maskedCells

def ndarray_to_png(data):
    # Convenience function to convert ndarray to a png-writable format from
    # https://stackoverflow.com/questions/6915106/saving-a-numpy-array-as-an-image-instructions
    rescaled = (255.0 / data.max() * (data - data.min())).astype(np.uint8)
    return(Image.fromarray(rescaled))

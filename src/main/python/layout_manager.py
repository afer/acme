#!/usr/bin/env python
from PySide2.QtCore import QObject, Signal, Slot  

class LayoutController(QObject):
'''
Coordinates the layout menu with the canvas_manager visualisations
'''

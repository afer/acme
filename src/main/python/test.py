from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtWidgets import (QLabel, QGridLayout, QListWidgetItem,
                               QListWidget)
from PySide2.QtCore import QObject, Signal, Slot  #, QThread
import sys
from pos_manager import PosController

app = QtWidgets.QApplication(sys.argv)

metadata_fields = ['position', 'trap', 'tp']
# metadata_fields = ['experimentID', 'position', 'trap', 'tp']
l = PosController(metadata_fields)
l.load_input("/home/alan/Desktop/PhD/softDev/annotGUI/curationGUI/testPoses/")

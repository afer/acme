from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Slot

class messageBox(QtWidgets.QLabel):
    def __init__(self):
        super().__init__()
        self.setText('Drawing initialised')

    @Slot(str)
    def mode_changed_msg(self, new_mode):
        self.update_message('Mode changed to ' + new_mode)

    @Slot(str)
    def fitted_outline_msg(self):
        self.update_message('Outline fitted')

    @Slot(str)
    def update_message(self, message):
        self.setText(message)
        self.update()
